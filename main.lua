require "fly"
require "perlin"

function love.load()
   love.window.setTitle("Movement")
   love.graphics.setBackgroundColor(3/255, 182/255, 252/255)
   background = love.graphics.newImage("resources/background.png")
   love.window.setMode(background:getWidth(), background:getHeight())
   
--    f = Fly:create("resources/mosquito_small.png", 100, 100, 2)
   
h = HerdFlies:create("resources/mosquito_small.png", 0, 0, 200, 200, 1000)
end

function love.draw()
    love.graphics.draw(background, 0, 0)
    love.graphics.print("FPS = "..tostring(love.timer.getFPS(), 10, 10))
    h:draw()    
end

function love.update()
    
    h:update()
end
