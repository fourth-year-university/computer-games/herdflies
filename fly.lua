HerdFlies = {}
HerdFlies.__index = HerdFlies

function HerdFlies:create(path, xmin, xmax, ymin, ymax, n)
    local flies = {}
    setmetatable(flies, HerdFlies)
    flies.n = n
    flies.herd = {}
    for i=1, n do
        x = love.math.random(xmin, xmax)
        y = love.math.random(ymin, ymax)
        speed = love.math.random(1, 3)
        flies.herd[i] = Fly:create(path, x, y, speed)
    end
    return flies
end

function HerdFlies:update()
    for i=1, self.n do
        self.herd[i]:update()
    end
end

function HerdFlies:draw()
    for i=1, self.n do
        self.herd[i]:draw()
    end
end

Fly = {}
Fly.__index = Fly

function Fly:create(path, x, y, speed)
    local fly = {}
    setmetatable(fly, Fly)
    fly.path = path
    fly.x = x or 0
    fly.y = y or 0
    fly.speed = speed or 1
    fly.image = love.graphics.newImage(fly.path)
    fly.noisex = Noise:create(1, 1, 256)
    fly.noisey = Noise:create(1, 1, 256)
    fly.tx = love.math.random(1, 100)
    fly.ty = love.math.random(1, 100)
    return fly
end

function Fly:update()
--     step = love.math.random()
--     if step < 0.5 then
--         self.x = self.x + self.speed
--     elseif step >= 0.5 and step <= 0.65 then
--         self.x = self.x - self.speed
--     elseif step >= 0.65 and step <= 0.85 then
--         self.y = self.y + self.speed
--     else
--         self.y = self.y - self.speed
--     end
    self.tx = self.tx + 0.01
    self.ty = self.ty + 0.015
    self.x1 = self.noisex:call(self.tx)
    self.y1 = self.noisey:call(self.ty)
    self.x = remap(self.x1, 0, 1, 50, background:getWidth() - 50)
    self.y = remap(self.y1, 0, 1, 50, background:getHeight() - 50)
end

function Fly:draw()
    love.graphics.draw(self.image, self.x, self.y)
end